# README #

This repo is a demo of code for a Visual Studio 2013 solution Matt Del Vecchio is working on. It contains three projects: 

* Common - shared helper classes
* IncidentReports - main web UI
* IncidentReports.DataAcces - DAL

### What is this repository for? ###

* just a a sample of some code

