﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReportHq.Common
{
	/// <summary>
	/// A helper class for writing to log files. (can also implement reading methods if desired)
	/// </summary>
	public class Logger
	{
		private FileStream _fileStream;

		#region Constructors

		public Logger(string fullPath)
		{
			_fileStream = CreateLogFile(fullPath);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		/// <summary>
		/// Appends entry to the filestream for an instantiated Logger obect.
		/// </summary>
		/// <param name="message">Entry to write for this line.</param>
		public void WriteToLog(string message)
		{
			// make sure we can write to this stream
			if (!_fileStream.CanWrite)
			{
				// close it and reopen for append
				string fileName = _fileStream.Name;
				_fileStream.Close();
				_fileStream = new FileStream(fileName, FileMode.Append);
			}

			StreamWriter streamWriter = new StreamWriter(_fileStream);
			streamWriter.WriteLine(String.Format("{0:G}: {1}", DateTime.Now, message));
			streamWriter.Close();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Closes the current stream and releases any resources (such as sockets and file handles) associated with the current stream.
		/// </summary>
		public void Close()
		{
			_fileStream.Close();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Static Methods

		/// <summary>
		/// Creats a writable FileStream to the supplied log file. Specifiy unique names to create new files, or use the same name to append. GetDateTimeAsFileName() and GetDateAsFileName() can assist in creating names.
		/// </summary>
		/// <param name="logFileName">Name to use for the new file</param>
		public static FileStream CreateLogFile(string fileName)
		{
			FileStream fileStream;

			//if file exists, append to it. if not, create it.
			if (File.Exists(fileName))
				fileStream = new FileStream(fileName, FileMode.Append);
			else
				fileStream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.None);

			return (fileStream);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Writes entries to the supplied log file filestream.
		/// </summary>
		/// <param name="fileStream">FileStream of the log file.</param>
		/// <param name="message">Entry to write for this line.</param>
		public static void WriteToLog(FileStream fileStream, string message)
		{
			// make sure we can write to this stream
			if (!fileStream.CanWrite) //if not..
			{
				//..close it and reopen for an append
				string fileName = fileStream.Name;
				fileStream.Close();
				fileStream = new FileStream(fileName, FileMode.Append);
			}

			StreamWriter streamWriter = new StreamWriter(fileStream);
			streamWriter.WriteLine(String.Format("{0:G}: {1}", DateTime.Now, message));
			streamWriter.Close();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Returns a unique time-based filename for a log. Ex: "2014-05-19_132807.txt"
		/// </summary>
		/// <returns></returns>
		public static string GetDateTimeAsFileName()
		{
			return String.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd_HHmmss"));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Returns a date-based filename for a log. Ex: "2014-05-19.txt"
		/// </summary>
		/// <returns></returns>
		public static string GetDateAsFileName()
		{
			return String.Format("{0}.txt", DateTime.Now.ToString("yyyy-MM-dd"));
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
