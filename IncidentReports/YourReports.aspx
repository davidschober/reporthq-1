﻿<%@ Page Title="Your Reports" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="YourReports.aspx.cs" Inherits="ReportHq.IncidentReports.YourReports" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">
		
	<%--<p>[start & end date pickers here]   [GO]</p>--%>

	<asp:Label ID="lblResultCount" runat="server" />

	<telerik:RadAjaxPanel ID="updResults" LoadingPanelID="ajaxLoadingPanel" ClientEvents-OnRequestStart="onRequestStart" runat="server">

		<telerik:RadGrid ID="gridReports"
			Width="95%"
			AllowPaging="true"
			PageSize="25"
			AllowSorting="true"
			AutoGenerateColumns="False"
			AllowMultiRowSelection="false"
			AllowMultiRowEdit="false"
			EnableHeaderContextMenu="true"
			OnNeedDataSource="gridReports_NeedDataSource"
			OnDetailTableDataBind="gridReports_DetailTableDataBind"
			runat="server">

			<ClientSettings 
				EnableRowHoverStyle="true"
				AllowDragToGroup="false" 
				AllowColumnsReorder="false" 
				AllowKeyboardNavigation="true"
				ReorderColumnsOnClient="false" >
				<Resizing AllowColumnResize="false" AllowRowResize="false" />
				<Selecting AllowRowSelect="false" EnableDragToSelectRows="false" />
			</ClientSettings>

			<ExportSettings FileName="myReports" ExportOnlyData="true" IgnorePaging="true" HideStructureColumns="true" OpenInNewWindow="true">
				<Excel Format="ExcelML"></Excel>
			</ExportSettings>
			
			<MasterTableView 
				CommandItemDisplay="Bottom"
				DataKeyNames="ReportID" 
				NoMasterRecordsText="No Reports to display." 
				CommandItemStyle-Font-Bold="true" 
				HeaderStyle-ForeColor="#191970" 
				ItemStyle-CssClass="item" 
				AlternatingItemStyle-CssClass="item">
				
				<PagerStyle PageSizes="25,50,100" Position="TopAndBottom" />
				<CommandItemSettings ShowRefreshButton="false" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
				
				<Columns>				
					<Telerik:GridTemplateColumn UniqueName="PdfIcon" ItemStyle-HorizontalAlign="center">
						<ItemTemplate>
							<a href='print/printReport.aspx?rid=<%# Eval("ReportID") %>'><img src="images/pdf.gif" width="16" height="16" border="0" alt="View PDF" /></a>
						</ItemTemplate>
					</Telerik:GridTemplateColumn>
					<Telerik:GridHyperLinkColumn UniqueName="ViewReport" DataNavigateUrlFields="ReportID" DataNavigateUrlFormatString="Event/BasicInfo.aspx?rid={0}" DataTextField="ReportID" DataTextFormatString="View" ItemStyle-HorizontalAlign="center" ItemStyle-Wrap="false" ItemStyle-CssClass="commandItem" />
					<Telerik:GridBoundColumn DataField="ReportID" Visible="false" />
					<Telerik:GridBoundColumn HeaderText="Item #" UniqueName="ItemNumber" DataField="ItemNumber" ItemStyle-Wrap="false" />
					<Telerik:GridBoundColumn HeaderText="Type" UniqueName="ReportType" DataField="ReportType" ItemStyle-Wrap="false" />
					<Telerik:GridBoundColumn HeaderText="Status" UniqueName="Status" DataField="ApprovalStatus" ItemStyle-Wrap="false" />
					<Telerik:GridBoundColumn HeaderText="Signal" UniqueName="Signal" DataField="Signal" HeaderStyle-Wrap="false" />
					<Telerik:GridBoundColumn HeaderText="Report Date" UniqueName="ReportDate" DataField="ReportDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Wrap="false" />
					<Telerik:GridBoundColumn HeaderText="District" UniqueName="District" DataField="District" HeaderStyle-HorizontalAlign="center" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="center" />
					<Telerik:GridBoundColumn HeaderText="Last Modified" UniqueName="LastModifiedDate" DataField="LastModifiedDate" DataFormatString="{0:MM/dd/yyyy}" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
				</Columns>
				
				<DetailTables>
			
					<%-- PERSONS --%>
					<Telerik:GridTableView 
						Name="persons" 
						DataKeyNames="VictimPersonID" 
						AutoGenerateColumns="false" 
						AllowSorting="false" 
						Caption="VICTIM/PERSONS" 
						Width="100%" 
						NoDetailRecordsText="No Victims/Persons to display">
						
						<ParentTableRelation>
							<Telerik:GridRelationFields DetailKeyField="ReportID" MasterKeyField="ReportID" />
						</ParentTableRelation>
						
						<Columns>
							<Telerik:GridBoundColumn HeaderText="ReportID" DataField="ReportID" Visible="false" />
							<Telerik:GridBoundColumn HeaderText="PersonID" DataField="PersonID" Visible="false" />
							<Telerik:GridBoundColumn HeaderText="Victim #" DataField="VictimNumber" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
							<Telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" />
							<Telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" />
							<Telerik:GridBoundColumn HeaderText="Race" DataField="Race" />
							<Telerik:GridBoundColumn HeaderText="Gender" DataField="Gender" />
							<Telerik:GridBoundColumn HeaderText="DOB" DataField="DateOfBirth" DataFormatString="{0:MM/dd/yyyy}" />
						</Columns>
					</Telerik:GridTableView>
					
					<%-- OFFENDERS --%>
					<Telerik:GridTableView 
						Name="offenders" 
						DataKeyNames="OffenderID" 
						AutoGenerateColumns="false" 
						AllowSorting="false" 
						Caption="OFFENDERS" 
						Width="100%" 
						NoDetailRecordsText="No Offenders to display" 
						CssClass="setMargin">
						
						<ParentTableRelation>
							<Telerik:GridRelationFields DetailKeyField="ReportID" MasterKeyField="ReportID" />
						</ParentTableRelation>
						<Columns>
							<Telerik:GridBoundColumn HeaderText="ReportID" DataField="ReportID" Visible="false" />
							<Telerik:GridBoundColumn HeaderText="OffenderID" DataField="OffenderID" Visible="false" />
							<Telerik:GridBoundColumn HeaderText="Offender #" DataField="OffenderNumber" ItemStyle-Width="70" HeaderStyle-HorizontalAlign="center" ItemStyle-HorizontalAlign="center" />
							<Telerik:GridBoundColumn HeaderText="Last Name" DataField="LastName" />
							<Telerik:GridBoundColumn HeaderText="First Name" DataField="FirstName" />
							<Telerik:GridBoundColumn HeaderText="Race" DataField="Race" />
							<Telerik:GridBoundColumn HeaderText="Gender" DataField="Gender" />
							<Telerik:GridBoundColumn HeaderText="DOB " DataField="DateOfBirth" DataFormatString="{0:MM/dd/yyyy}" />
						</Columns>
					</Telerik:GridTableView>

				</DetailTables>
				
			</MasterTableView>
		</Telerik:RadGrid>

	</telerik:RadAjaxPanel>

</asp:Content>
