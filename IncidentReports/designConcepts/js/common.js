﻿//jquery event handler to close menus appropriately
$(document).mouseup(function (e) {

	var targetID = e.target.id;
	//alert(targetID);

	switch (targetID) {
		//if clicking the user menu, close the notifications menu
		case "UserLink":
		case "UserIcon":
			closeMenu("#Notifications", null, false);
			break;

		//if clicking the notifications menu, close the user menu
		case "NotificationsImage":
			closeMenu("#UserMenu", null, false);
			break;

		//if the click is anywhere else, close both
		default:
			closeMenu("#Notifications", e.target, true);
			closeMenu("#UserMenu", e.target, true);
			break;
	}
});

function toggleMenu(divId) {
	var container = $(divId);
	container.fadeToggle(75, "linear");
}

function showNotifications() {
	toggleMenu("#Notifications"); //toggle menu
	$('#NotificationsCount').hide(); //reset count (NOTE: would have to clear in db as well via webcall)			
}

function closeMenu(divId, target, bCheckIfTargetIsSelfOrChild) {

	var container = $(divId);

	//we dont want to close the menu if the click target is in the menu itself (or its children). so check for that & exclude
	if (bCheckIfTargetIsSelfOrChild) {
		if (!container.is(target) && container.has(target).length === 0) {
			container.fadeOut(75, "linear");
		}
	} else {
		container.fadeOut(75, "linear");
	}

}