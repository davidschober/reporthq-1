﻿<%@ Page Title="test" Language="C#" AutoEventWireup="true" CodeBehind="test.aspx.cs" Inherits="ReportHq.IncidentReports.test" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Interactive Field Map Test</title>
</head>

<body>

	<form id="form1" runat="server">

		<telerik:RadScriptManager ID="radScriptManager" runat="server" />
		
	
		<telerik:RadLightBox ID="lightBox" LoopItems="true" runat="server">
			<ClientSettings AllowKeyboardNavigation="true" ContentResizeMode="Fit" NavigationMode="Zone" />
		</telerik:RadLightBox>
		
		<a href="#" onclick="openLightbox(0);">File Name 1.jpg</a><br />
		<a href="#" onclick="openLightbox(1);">File Name 2.jpg</a><br />
		<a href="#" onclick="openLightbox(2);">File Name 3.jpg</a><br />

	

		<script type="text/javascript">

			function openLightbox(i) {
				var lightBox = $find('<%= lightBox.ClientID %>');
			
				//TODO: set current item index
				lightBox.show();
			}

		</script>

	</form>

</body>
</html>
