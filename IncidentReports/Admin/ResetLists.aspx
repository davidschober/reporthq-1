﻿<%@ Page Title="Admin Tools" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="ResetLists.aspx.cs" Inherits="ReportHq.IncidentReports.Admin.ResetLists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<p>Zaps all saved list tables (ex: Signals, Car Makes, Sobriety Types, etc) from the server's cache resevoir. Default time-to-live is 7 days.</p>

	<p>&nbsp;</p>

	<p>
		<asp:LinkButton ID="btnClearCahe" CssClass="flatButton" runat="server" OnClick="btnClearCahe_Click">
			<i class="fa fa-bolt fa-fw"></i>Clear Cache
		</asp:LinkButton>
	</p>

</asp:Content>
