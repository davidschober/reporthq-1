﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResetNotifications.aspx.cs" Inherits="ReportHq.IncidentReports.Admin.ResetNotifications" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Reset Notifications (dev)</title>
	<link rel="stylesheet" href="../fonts/webfonts.css" type="text/css" />
	<link rel="stylesheet" href="~/css/popup.css" type="text/css" />
	<link rel="stylesheet" href="~/css/common.css" type="text/css" />

	<style>

		html, form, body {
			margin:5px;
			height:auto;
		}

	</style>

</head>
<body>
    <form id="form1" runat="server">
    <div>

		<h2>Demo tools:</h2>
    
		<asp:LinkButton ID="lbResetNotifications" ToolTip="sets all user notifications as un-read" CssClass="flatButton big" OnClick="lbResetNotifications_Click" runat="server">Reset Notifications</asp:LinkButton>
				

    </div>
    </form>
</body>
</html>
