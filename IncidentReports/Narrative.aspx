﻿<%@ Page Title="Report HQ - Narrative" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Narrative.aspx.cs" Inherits="ReportHq.IncidentReports.Narrative" %>
<%@ Register TagPrefix="ReportHq" TagName="Narrative" src="controls/Narrative.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link type="text/css" rel="stylesheet" href="../css/reportForm.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Narrative" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Narrative ID="ucNarrative" OnSaveButtonClicked="ucNarrative_SaveButtonClicked" runat="server" />

</asp:Content>
