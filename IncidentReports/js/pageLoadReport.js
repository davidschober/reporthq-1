// - used by various pages to recall whether certain RadWindows are open/closed, and where on the screen.
// - it is also used to maintain the applications Timeout for session renewal.

//this is a built-in .NET/Telerik event handler that gets called automagically.
function pageLoad()  
{
	var reportID = document.getElementById('hidReportID').value;
	//alert(reportID);

	//get reportID from page, append to cookie names
	checkAndOpenRadWindow('rwSupervisorNotes', 'superNotesVis_' + reportID, 'superNotesCoords_' + reportID);
	//checkAndOpenRadWindow(rwSupervisorReview', 'superVisible', 'superCoords')
	
	//app session is 60 minutes. so start a timer that counts down for 59 mins (3540 secs), then calls openSessionRenew();
	setTimeout('openRenewSession()', 3540000); //in milliseconds
} 
