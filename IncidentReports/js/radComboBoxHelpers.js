
//used by RadComboBoxes to auto-display items on focus.
function showDropDown(sender)
{  
	var inputArea = sender.get_inputDomElement();
	sender.showDropDown();
}

//used by RadComboBoxes to indicate required fields.
function setColor(sender)  
{  	
	var inputArea = sender.get_inputDomElement();
	
	if (sender._value != '')
	{		
		inputArea.style.backgroundColor = '';
	}
	else
	{
		inputArea.style.backgroundColor = 'yellow';
	}
	
	/*
		in ASPX:
		OnClientBlur="setColor" 
	
		in Page_Load:
		if (rcbReportStatusTypes.SelectedValue == string.Empty)
				rcbReportStatusTypes.BackColor = System.Drawing.Color.Yellow;
	*/
}
