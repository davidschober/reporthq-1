function openBulletin(bulletinID) {
	radopen('/ViewBulletin.aspx?bid=' + bulletinID, 'rwBulletin');
}

//provides a reference to the RadWindow "wrapper"
function getRadWindow()
{
	var oWindow = null;
	
	if (window.radWindow) 
		oWindow = window.radWindow; //Will work in Moz in all cases, including clasic dialog
	else if (window.frameElement.radWindow) 
		oWindow = window.frameElement.radWindow;//IE (and Moz az well)
	
	return oWindow;
}

function openRadWindow(windowName) 
{
	radopen(null, windowName);	
}

function closeRadWindow()
{
	getRadWindow().Close();
}

function redirectParentPage(url)
{
	getRadWindow().BrowserWindow.location = url;
}

function refreshParentPage()
{
	getRadWindow().BrowserWindow.location.reload();
}