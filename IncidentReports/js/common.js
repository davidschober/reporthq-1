﻿//jquery event handler to close menus appropriately
$(document).mouseup(function (e) {

	var targetID = e.target.id;
	//alert(targetID);

	switch (targetID) {
		//if clicking the user menu, close the notifications menu
		case "UserLink":
		case "UserIcon":
			closeMenu("#Notifications", null, false);
			break;

		//if clicking the notifications menu, close the user menu
		case "NotificationsImage":
			closeMenu("#UserMenu", null, false);
			break;

		//if the click is anywhere else, close both
		default:
			closeMenu("#Notifications", e.target, true);
			closeMenu("#UserMenu", e.target, true);
			break;
	}
});

function toggleMenu(divId) {
	var container = $(divId);
	container.fadeToggle(75, "linear");
}

function closeMenu(divId, target, bCheckIfTargetIsSelfOrChild) {

	var container = $(divId);

	//we dont want to close the menu if the click target is in the menu itself (or its children). so check for that & exclude
	if (bCheckIfTargetIsSelfOrChild) {
		if (!container.is(target) && container.has(target).length === 0) {
			container.fadeOut(75, "linear");
		}
	} else {
		container.fadeOut(75, "linear");
	}

}

//sets a control's value to uppercase
function upperCaseIt(control) {
	control.value = control.value.toUpperCase();
}

//doesnt let a textbox enter more than max length (used by multi-lines)
function isMaxLength(textbox, maxLength) {
	return (textbox.value.length < maxLength);
}

//trims a textbox to max length (used by multi-lines)
function trimSize(textbox, maxLength) {
	if (textbox.value.length > maxLength) {
		textbox.value = textbox.value.substring(0, maxLength);
		alert('Text trimmed to ' + maxLength + ' characters.');
	}
}

function getShortDate(dateString) {
	var date = new Date(dateString);
	return (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
}

//if the supplied number is less than 5-digits, pads with zeros
function padNumberSection(sender) {
	var userValue = sender.get_valueWithPromptAndLiterals(); // 'K-2____-08'

	var arrParts = userValue.split('-');
	var middlePart = replace(arrParts[1], '_', ''); //swap "_" for ""
	var endPart = replace(arrParts[2], '_', '');

	//if less than 5 chars, pad to the left w/ 0s
	while (middlePart.length < 5)
		middlePart = '0' + middlePart;

	//if less than 2 chars, pad to the left w/ 0s
	while (endPart.length < 2)
		endPart = '0' + endPart;

	var newValue = arrParts[0] + middlePart + endPart;

	//set new value back to form's value
	if (middlePart != '00000')
		sender.set_value(newValue);
}

//handles click for panel expand/collapse (TODO: move to shared .js)
function toggleCollapseState(sElementName) {

	var element = $(sElementName);
	var parent = element.parent();

	//vis state:
	var isVisible = element.is(':visible');

	if (isVisible == true)
		parent.find('dt').find('i').removeClass().addClass('fa fa-chevron-circle-down');
	else
		parent.find('dt').find('i').removeClass().addClass('fa fa-chevron-circle-up');

	element.slideToggle();
	parent.toggleClass('collapsed');
}

//allows ajaxified RadGrid to export to excel
function onRequestStart(sender, args) {
	if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0)
		args.set_enableAjax(false);
}