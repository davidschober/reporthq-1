﻿<%@ Page Title="Report HQ - Attachments" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Attachments.aspx.cs" Inherits="ReportHq.IncidentReports.Attachments" %>
<%@ Register TagPrefix="ReportHq" TagName="Attachments" src="controls/Attachments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<link rel="stylesheet" href="../css/boxes.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Attachments" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Attachments ID="ucAttachments" runat="server" />

</asp:Content>
