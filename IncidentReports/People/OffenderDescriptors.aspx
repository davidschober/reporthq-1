﻿<%@ Page Title="Report HQ - Descriptors" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="OffenderDescriptors.aspx.cs" Inherits="ReportHq.IncidentReports.People.OffenderDescriptors" %>
<%@ Register TagPrefix="ReportHq" TagName="OffenderDescriptors" src="controls/OffenderDescriptors.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="People" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:OffenderDescriptors ID="ucOffenderDescriptors"  OnNavItemClicked="OffenderDescriptors_NavItemClicked" runat="server" />

	<telerik:RadAjaxManager ID="ajaxManager" DefaultLoadingPanelID="ajaxLoadingPanel" runat="server" />

</asp:Content>
