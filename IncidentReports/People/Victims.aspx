﻿<%@ Page Title="Report HQ - Victims" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Victims.aspx.cs" Inherits="ReportHq.IncidentReports.People.Victims" %>
<%@ Register TagPrefix="ReportHq" TagName="Victims" src="controls/Victims.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="People" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Victims ID="ucVictims" runat="server" />

</asp:Content>
