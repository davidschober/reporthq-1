﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportSubNav.ascx.cs" Inherits="ReportHq.IncidentReports.People.Controls.ReportSubNav" %>

<!--
<div id="ReportSubNav">
	<a href="#">Area 1</a>
	<a href="#">Area 2</a>
	<span class="selected">Area 3</span>
	<a href="#">Area 4</a>				
</div>
-->


<div id="ReportSubNav">
	<asp:Label ID="lblDescriptors" Text="Descriptors" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbDescriptors" Text="Descriptors" ToolTip="Save your work and move here" OnClick="lbDescriptors_Click" runat="server" />
	<asp:Label ID="lblOffenders" Text="Offenders" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbOffenders" Text="Offenders" ToolTip="Save your work and move here" OnClick="lbOffenders_Click" runat="server" />
	<asp:Label ID="lblVictims" Text="Victims" CssClass="selected" Visible="false" runat="server" /><asp:LinkButton ID="lbVictims" Text="Victims" ToolTip="Save your work and move here" OnClick="lbVictims_Click" runat="server" />
</div>