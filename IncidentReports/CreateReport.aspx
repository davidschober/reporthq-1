﻿<%@ Page Title="Create Report" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="CreateReport.aspx.cs" Inherits="ReportHq.IncidentReports.CreateReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script type="text/javascript" src="js/common.js"></script>
	<link type="text/css" rel="stylesheet" href="css/reportForm.css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">
		
	<div class="formBox" style="width:260px; min-height:initial;">
	
		<table border="0">
			<tr>
				<td class="label" nowrap="nowrap">Item Number:</td>
				<td>
					<Telerik:RadMaskedTextBox id="rmtbItemNumber" Mask="L-#####-##"  ClientEvents-OnBlur="padNumberSection" width="110" runat="server" />
					<asp:RequiredFieldValidator id="rfvItemNumber" ControlToValidate="rmtbItemNumber" ValidationGroup="create" ErrorMessage="Item Number required" Display="none" runat="server" />
					<asp:RegularExpressionValidator 
						id="revItemNumber" 
						ControlToValidate="rmtbItemNumber" 
						ValidationGroup="create"						
						ErrorMessage="Item Number invalid"						
						ValidationExpression="[A-L]-[0-9]{5}-[0-9]{2}" 						
						Display="none" 
						runat="server" />
				</td>
			</tr>
			<tr>
				<td class="label" nowrap="nowrap">Report Type:</td>
				<td><Telerik:RadComboBox ID="rcbReportTypes" DataValueField="ReportTypeID" DataTextField="Description" width="110" AllowCustomText="False" MarkFirstMatch="true" runat="server" /></td>
			</tr>
		</table><br />
		
		<asp:ValidationSummary ID="validationSummary" ValidationGroup="create" DisplayMode="bulletList" CssClass="validationSummary" EnableClientScript="true" runat="server" />
		
		<asp:LinkButton ID="lbSubmit" Text="Create Report" ValidationGroup="create" OnClientClick="confirmSubmit(this); return toSubmit;" OnClick="lbSubmit_Click" CssClass="flatButton" runat="server" />
		
	</div>


	<script type="text/javascript">
	
		var toSubmit = false;
		
		function confirmSubmit(button)  
		{
			if(toSubmit) return;

			var rmtbItemNumber = $find("<%= rmtbItemNumber.ClientID %>");
			function doClick(arg)
			{  
				//alert(arg);
				if(arg)
				{
					toSubmit = true;
					var btnSubmit = $get('<%= lbSubmit.ClientID %>');
					btnSubmit.click();
				}
			}	
			radconfirm("<p>Are you sure <span style='font-size:18px;color:red'>" +  rmtbItemNumber.get_valueWithLiterals() + "</span> is the correct Incident Number?</p>", doClick);
		}
		
		/*		
		function confirmSubmit()
		{
			if (confirm('Are you sure this is the *correct* Incident Number? This cannot be changed later.'))
				return true;
			else
				return false;
		}
		*/		
	</script>


</asp:Content>
