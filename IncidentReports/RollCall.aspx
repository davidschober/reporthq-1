﻿<%@ Page Title="Roll Call" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="RollCall.aspx.cs" Inherits="ReportHq.IncidentReports.RollCall" %>
<%@ Register TagPrefix="ReportHq" TagName="Apbs" src="controls/APBs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="css/boxes.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<%--<a id="lnkCreateBulletin" href="supervisor/CreateBulletin.aspx" title="Create a new Bulletin" class="flatButton" visible="false" runat="server"><i class="fa fa-plus"></i> Create Bulletin</a>--%>
			
	<div style="margin-top:7px;">
		
		<!-- APBs -->
		<ReportHq:Apbs ID="ucApbs" runat="server" />


		<!-- BOLOs -->
		<dl class="box subtext">
			<dt>				
				<i class="iconExclaim"></i>
				<div class="description">
					<h3>BOLOs</h3>
					Be On the Look Outs
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptBolo" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th>DATE</th>
									<th>TYPE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%# string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%></a></td>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%#Eval("Type")%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNoneBolo" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>


		<!-- personnel -->
		<dl class="box subtext">
			<dt>				
				<i class="iconExclaim"></i>
				<div class="description">
					<h3>Personnel</h3>
					Personnel change bulletins
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptPersonnel" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th>DATE</th>
									<th>TITLE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%# string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%></a></td>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%#Eval("Title")%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNonePersonnel" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>


		<!-- policy -->
		<dl class="box subtext">
			<dt>				
				<i class="iconExclaim"></i>
				<div class="description">
					<h3>Policy</h3>
					Policy bulletins
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptPolicy" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th>DATE</th>
									<th>TITLE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%# string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%></a></td>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%#Eval("Title")%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNonePolicy" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>


		<!-- memo -->
		<dl class="box subtext">
			<dt>				
				<i class="iconExclaim"></i>
				<div class="description">
					<h3>Memos</h3>
					General memos
				</div>
			</dt>
			<dd>
				<asp:Repeater ID="rptMemo" runat="server">
					<HeaderTemplate>
						<table width="100%" cellspacing="0" border="0" class="lightList">
							<thead>
								<tr>
									<th>DATE</th>
									<th>TITLE</th>
								</tr>
							</thead>
							<tbody>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%# string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%></a></td>
							<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%#Eval("Title")%></a></td>
						</tr>
					</ItemTemplate>
					<FooterTemplate>
							</tbody>
						</table>
					</FooterTemplate>
				</asp:Repeater>
				<asp:Literal ID="litNoneMemo" Text="None at this time." Visible="false" runat="server" />
			</dd>
		</dl>

				
		<p style="clear:both;">&nbsp;</p>
		
	</div>

</asp:Content>
