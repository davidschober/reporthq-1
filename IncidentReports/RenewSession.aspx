﻿<%@ Page Title="Renew Session" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="RenewSession.aspx.cs" Inherits="ReportHq.IncidentReports.RenewSession" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<script src="js/radWindowHelpers.js" type="text/javascript"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<h1>Renew Session</h1>
	
	<asp:Label ID="lblMessage" EnableViewState="false" runat="server"><p>Your session has expired. You may renew it and continue working.</p><br /></asp:Label>
	
	<asp:LinkButton ID="lbSubmit" OnClick="lbSubmit_Click" CssClass="flatButton" runat="server"><i class="fa fa-history fa-fw" style="font-size:14px;"></i> Continue Working</asp:LinkButton>

	<asp:HiddenField ID="hidReportID" runat="server" />

</asp:Content>