﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ReportHq.IncidentReports.Property
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			//these two are supposed to help scability in a redirect
			Response.Redirect("Weapons.aspx", false); // endReponse = false lets us avoid ThreadAbortExceptions by letting the response finish
			HttpContext.Current.ApplicationInstance.CompleteRequest(); //causes the run-time to bypass all the events in the HTTP pipeline
		}
	}
}