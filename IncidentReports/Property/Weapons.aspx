﻿<%@ Page Title="Report HQ - Weapons" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Weapons.aspx.cs" Inherits="ReportHq.IncidentReports.Property.Weapons" %>
<%@ Register TagPrefix="ReportHq" TagName="Weapons" src="controls/Weapons.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Property" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Weapons ID="ucWeapons" runat="server" />

</asp:Content>
