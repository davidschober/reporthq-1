﻿<%@ Page Title="Report HQ - Property/Evidence" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="PropertyEvidence.aspx.cs" Inherits="ReportHq.IncidentReports.Property.PropertyEvidence" %>
<%@ Register TagPrefix="ReportHq" TagName="PropertyEvidence" src="controls/PropertyEvidence.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Property" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:PropertyEvidence ID="ucPropertyEvidence" runat="server" />

</asp:Content>
