﻿<%@ Page Title="Report HQ - Vehicles" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="Vehicles.aspx.cs" Inherits="ReportHq.IncidentReports.Property.Vehicles" %>
<%@ Register TagPrefix="ReportHq" TagName="Vehicles" src="controls/Vehicles.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Property" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:Vehicles ID="ucVehicles" runat="server" />

</asp:Content>
