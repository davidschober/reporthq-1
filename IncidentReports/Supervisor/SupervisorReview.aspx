﻿<%@ Page Title="Supervisor Review" Language="C#" MasterPageFile="~/Popup.Master" AutoEventWireup="true" CodeBehind="SupervisorReview.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.SupervisorReview" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link type="text/css" rel="stylesheet" href="../css/reportForm.css" />
	<script src="../js/radComboBoxHelpers.js" type="text/javascript"></script>
	<script src="../js/radWindowHelpers.js" type="text/javascript"></script>

	<style>
		body {
			padding:10px;
		}
	</style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="RadioButtons" Skin="Metro" runat="server" />

	<div class="formBox" style="width:550px; left:initial;">

		<table border="0">
			<tr>
				<td class="label">Rank:</td>
				<td colspan="4">
					<Telerik:RadComboBox ID="rcbSupervisorRank" DataValueField="RankTypeID" DataTextField="Description" Width="150" MarkFirstMatch="true" OnClientFocus="showDropDown" runat="server" />
					<asp:RequiredFieldValidator ID="ffvSupervisorRank" ControlToValidate="rcbSupervisorRank" ValidationGroup="supervisor" ErrorMessage="Rank" Display="none" runat="server" />
				</td>
			</tr>
			<tr>
				<td class="label">Name:</td>
				<td colspan="4">
					<asp:TextBox ID="tbSupervisorName" MaxLength="50" Width="146" ReadOnly="true" runat="server" />
					<asp:RequiredFieldValidator ID="rfvName" ControlToValidate="tbSupervisorName" ValidationGroup="supervisor" ErrorMessage="Name" Display="none" runat="server" />
				</td>
			</tr>
			<tr>
				<td class="label">Badge #:</td>
				<td colspan="4">
					<Telerik:RadNumericTextBox ID="rntbSupervisorBadge" type="Number" MaxLength="5" MinValue="1" MaxValue="10999" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" Width="62" runat="server"/>
					<asp:RequiredFieldValidator ID="rfvBadge" ControlToValidate="rntbSupervisorBadge" ValidationGroup="supervisor" ErrorMessage="Badge #" Display="none" runat="server" />
				</td>
			</tr>
			<tr>
				<td class="label" nowrap="nowrap">Approval Status:</td>
				<td>
					<asp:RadioButtonList ID="rblApprovalStatus" RepeatDirection="horizontal" RepeatLayout="Flow" runat="server">
						<asp:ListItem Value="1" Text="Approved" />
						<asp:ListItem Value="2" Text="Disapproved" />
					</asp:RadioButtonList>
					<asp:RequiredFieldValidator ID="rfvStatus" ControlToValidate="rblApprovalStatus" ValidationGroup="supervisor" ErrorMessage="Approval Status" Display="none" runat="server" />
				</td>
			</tr>
			<tr>
				<td valign="top" class="label">Comments:</td>
				<td><asp:TextBox ID="tbComments" TextMode="MultiLine" Rows="10" Columns="50" MaxLength="500" onblur="upperCaseIt(this);" runat="server"/></td>
			</tr>
		</table><br />
		
		<asp:ValidationSummary ID="validationSummary" ValidationGroup="supervisor" DisplayMode="bulletList" CssClass="validationSummary" HeaderText="Required Fields:" EnableClientScript="true" runat="server" />
		
		<asp:LinkButton ID="lbSubmit" OnClientClick="return validateAndConfirm();" OnClick="lbSubmit_Click" CssClass="flatButton" ValidationGroup="supervisor" runat="server"><i class="fa fa-check fa-fw"></i> Save</asp:LinkButton> &nbsp;
		<asp:LinkButton ID="lbCancel" Text="Cancel" CssClass="flatButton" OnClientClick="closeRadWindow();" runat="server" />

		<p><asp:Literal id="litSaving" visible="false" runat="server">Saving...</asp:Literal></p>

	</div>

	<script type="text/javascript">

		var toSubmit = false; //used by submit button confirmation popup
		
		//TODO: why not use same cookie system as supervisor notes?

		////used by window management which keeps this supervisor window visible & remembers its location when bopping around:
		//var visibleName = 'superVis';
		//var coordsName = 'superOptCoords';
		
		//function resetCookies(closeWindow)
		//{
		//	deleteCookie(visibleName, '/', '');
		//	deleteCookie(coordsName, '/', '');
			
		//	if (closeWindow)
		//		closeRadWindow();
		//}

		//-------------------------------------------

		function validateAndConfirm()
		{
			var returnValue = false;

			//do asp.net clientside validation
			Page_ClientValidate();
			returnValue = Page_IsValid; //true if valid

			if (returnValue == true) {
				returnValue = confirm('Are you sure ' + ($get("phContent_tbSupervisorName")).value + ' is the Supervisor? This cannot be changed later.');
			}
			
			return returnValue;
		}
	</script>

</asp:Content>