﻿<%@ Page Title="Create Bulletin" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="CreateBulletin.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.CreateBulletin" %>
<%@ Register TagPrefix="ReportHq" TagName="BulletinEditor" src="Controls/BulletinEditor.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:BulletinEditor ID="ucBulletinEditor" runat="server" />

	<asp:LinkButton ID="lbSend" ValidationGroup="bulletin" OnClick="lbSend_Click" CssClass="flatButton" runat="server"><i class="fa fa-check fa-fw"></i> Send Bulletin</asp:LinkButton>

</asp:Content>
