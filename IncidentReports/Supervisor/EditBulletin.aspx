﻿<%@ Page Title="Edit Bulletin" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="EditBulletin.aspx.cs" Inherits="ReportHq.IncidentReports.Supervisor.EditBulletin" %>
<%@ Register TagPrefix="ReportHq" TagName="BulletinEditor" src="Controls/BulletinEditor.ascx" %>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:BulletinEditor ID="ucBulletinEditor" ShowActive="true" runat="server" />

	<asp:LinkButton ID="lbSave" ValidationGroup="bulletin" OnClick="lbSave_Click" ToolTip="Save your changes" CssClass="flatButton" runat="server"><i class="fa fa-check fa-fw"></i> Save Bulletin</asp:LinkButton>&nbsp;

	<a href="ManageBulletins.aspx" class="flatButton" title="Cancel your changes">Cancel</a>

</asp:Content>
