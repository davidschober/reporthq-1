using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class ReportNotification
	{
		#region Properties

		public int ID { get; set; }
		public int ReportID { get; set; }
		public string Recipient { get; set; }
		public string Message { get; set; }		
		//public DateTime CreatedDate { get; set; }

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when filling a new Notification from datarow. (ex: record from db).
		/// </summary>
		public ReportNotification(DataRow row)
		{
			FillObject(this, row);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when creating a new Notification.
		/// </summary>
		public ReportNotification(int reportID, string recipient, string message)
		{
			this.ReportID = reportID;
			this.Recipient = recipient;
			this.Message = message;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public void Send()
		{
			StatusInfo status = NotificationsDA.InsertReportNotification(this.ReportID, this.Recipient, this.Message);

			////TODO: if fails, send email to admin
			//if (!status.Success)
			//{

			//}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		/// <summary>
		/// Gets a collection of new (unread) ReportNotifications. To be used by the NotificationsController for web services.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		public static List<ReportNotification> GetNotifications(string username)		
		{
			List<ReportNotification> notifications = new List<ReportNotification>();

			DataTable dt = NotificationsDA.GetNewReportNotificationsByRecipient(username);
			foreach (DataRow row in dt.Rows)
			{
				notifications.Add(new ReportNotification(row));
			}

			return notifications;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo MarkAsRead(string username, string itemIDs)
		{
			return NotificationsDA.SetReportNotificationAsRead(username, itemIDs);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific GlobalNotification, via constructor.
		/// </summary>
		private void FillObject(ReportNotification notification, DataRow row)
		{
			//required
			notification.ID = (int)row["ID"];
			notification.ReportID = (int)row["ReportID"];
			notification.Message = (string)row["Message"];			
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
;