using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	/// <summary>
	/// Organizational reporting tasks -- user stats, dashboards, charts, etc.
	/// </summary>
	public class Reporting
	{
		#region Public Static Methods

		///// <summary>
		///// Tallies reports by District.
		///// </summary>
		//public static DataTable ReportsByDistrict(int reportTypeID, DateTime startDate, DateTime endDate)
		//{
		//	DateTime emptyDate = new DateTime(1900, 1, 1);

		//	//if user entered startdate but no enddate, assume today is enddate
		//	if (startDate != emptyDate && endDate == emptyDate)
		//		endDate = DateTime.Now;
			
		//	//by setting date to last minute of that day, we get all db records for that day back
		//	if (endDate != emptyDate)
		//	{
		//		endDate = endDate.AddHours(23);
		//		endDate = endDate.AddMinutes(59);
		//	}


		//	return ReportingDA.ReportsByDistrict(reportTypeID, startDate, endDate);
		//}

		////'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		///// <summary>
		///// Finds approved reports within a date range..
		///// </summary>
		//public static DataTable ApprovedReports(DateTime startDate, DateTime endDate)
		//{
		//	DateTime emptyDate = new DateTime(1900, 1, 1);

		//	//if user entered startdate but no enddate, assume today is enddate
		//	if (startDate != emptyDate && endDate == emptyDate)
		//		endDate = DateTime.Now;

		//	//by setting date to last minute of that day, we get all db records for that day back
		//	if (endDate != emptyDate)
		//	{
		//		endDate = endDate.AddHours(23);
		//		endDate = endDate.AddMinutes(59);
		//	}


		//	return ReportingDA.ApprovedReports(startDate, endDate);
		//}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets the user's stats for Rejected, Incomplete, Pending, Approved.
		/// </summary>
		public static DataTable DashboardStats(string username)
		{
			return ReportingDA.DashboardStats(username);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		/// <summary>
		/// Tallies pending Reports, per-district.
		/// </summary>
		public static DataTable PendingDistrictReports()
		{
			return ReportingDA.DistrictReportsByStatusType((int)Report.ApprovalStatusTypes.Pending);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Tallies pending Reports, per-district.
		/// </summary>
		public static DataTable RejectedDistrictReports()
		{
			return ReportingDA.DistrictReportsByStatusType((int)Report.ApprovalStatusTypes.Rejected);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Get tallies of approved Reports, per-district, by date range.
		/// </summary>
		public static DataTable ApprovedReportsByDistrict(DateTime startDate, DateTime endDate)
		{
			DateTime emptyDate = new DateTime(1900, 1, 1);

			//by setting date to last minute of that day, we get all db records for that day back
			if (endDate != emptyDate)
			{
				endDate = endDate.AddHours(23);
				endDate = endDate.AddMinutes(59);
			}

			return ReportingDA.ApprovedReportsByDistrict(startDate, endDate);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Tallies all approved district Reports, returns percentages by district.
		/// </summary>
		public static DataTable DistrictReportsPercentage()
		{
			return ReportingDA.DistrictReportsPercentage();
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
