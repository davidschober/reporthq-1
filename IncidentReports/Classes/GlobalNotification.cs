using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class GlobalNotification
	{
		#region Enums

		public enum Types
		{
			APB = 1,
			SystemNotification = 2
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Properties

		public int ID { get; set; }
		public int TypeID { get; set; }
		public string Type { get; set; }
		public int ObjectID { get; set; }
		public string Message { get; set; }
		public string Url { get; set; }
		public bool Viewed { get; set; }
		//public DateTime CreatedDate { get; set; }

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when creating a new Notification.
		/// </summary>
		public GlobalNotification(GlobalNotification.Types type, int objectID, string message, string url)
		{
			this.TypeID = (int)type;
			this.ObjectID = objectID; // 66
			this.Message = message; // "Child kidnapped"
			this.Url = url;  // "javascript:openBulletin(66);" or "/event/BasicInfo.aspx?rid=66"
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new Notification from datarow. (ex: record from db)
		/// </summary>
		public GlobalNotification(DataRow row)
		{
			FillObject(row);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public void Send(string username)
		{
			StatusInfo status = NotificationsDA.InsertGlobalNotification(this.TypeID, this.ObjectID, this.Message, this.Url, username);

			////TODO: if fails, send email to admin
			//if (!status.Success)
			//{

			//}
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		/// <summary>
		/// Gets a collection of new (unread) GlobalNotifications. To be used by the NotificationsController for web services.
		/// </summary>
		/// <param name="username"></param>
		/// <returns></returns>
		public static List<GlobalNotification> GetNotifications(string username)		
		{
			List<GlobalNotification> notifications = new List<GlobalNotification>();

			DataTable dt = NotificationsDA.GetNewGlobalNotificationsByRecipient(username);
			foreach (DataRow row in dt.Rows)
			{
				notifications.Add(new GlobalNotification(row));
			}

			return notifications;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo MarkAsRead(string username, string itemIDs)
		{
			return NotificationsDA.SetGlobalNotificationAsRead(username, itemIDs);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// TEMP only. reset notification read-states in order to have un-read notification badges show up in UI.
		/// </summary>
		public static void TempResetNotifications()
		{
			NotificationsDA.TempResetNotifications();
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific GlobalNotification, via constructor.
		/// </summary>
		private void FillObject(DataRow row)
		{
			//required
			this.ID = (int)row["GlobalNotificationID"];
			//notification.Type = (string)row["TypeDescription"];
			this.Message = (string)row["Message"];
			this.Url = (string)row["Url"];

			if (row["RelatedObjectID"] != DBNull.Value)
				this.ObjectID = (int)row["RelatedObjectID"];

			if (row["ViewedDate"] != DBNull.Value)
				this.Viewed = true;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
