using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class Weapon
	{
		#region Properties

		private int _ID;
		private int _reportID;
		private int _typeID;
		private string _type;
		private int _makeTypeID;
		private string _make;
		private string _model;
		private int _caliberTypeID;
		private string _caliber;
		private string _serialNumber;
		private bool _ncicIsStolen;
		private string _ncicContactName;
		private bool _hasPawnshopRecord;
		private string _pawnshopContactName;
		private string _additionalInfo;

		//private string _createdBy;
		//private DateTime _createdDate;
		//private string _lastModifiedBy;
		//private DateTime _lastModifiedDate;

		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		public int ReportID
		{
			set { _reportID = value; }
			get { return _reportID; }
		}

		public int TypeID
		{
			set { _typeID = value; }
			get { return _typeID; }
		}

		public string Type
		{
			set { _type = value; }
			get { return _type; }
		}

		public int MakeTypeID
		{
			set { _makeTypeID = value; }
			get { return _makeTypeID; }
		}

		public string Make
		{
			set { _make = value; }
			get { return _make; }
		}

		public string Model
		{
			set { _model = value; }
			get { return _model; }
		}

		public int CaliberTypeID
		{
			set { _caliberTypeID = value; }
			get { return _caliberTypeID; }
		}

		public string Caliber
		{
			set { _caliber = value; }
			get { return _caliber; }
		}

		public string SerialNumber
		{
			set { _serialNumber = value; }
			get { return _serialNumber; }
		}

		public bool NcicIsStolen
		{
			set { _ncicIsStolen = value; }
			get { return _ncicIsStolen; }
		}

		public string NcicContactName
		{
			set { _ncicContactName = value; }
			get { return _ncicContactName; }
		}

		public bool HasPawnshopRecord
		{
			set { _hasPawnshopRecord = value; }
			get { return _hasPawnshopRecord; }
		}

		public string PawnshopContactName
		{
			set { _pawnshopContactName = value; }
			get { return _pawnshopContactName; }
		}

		public string AdditionalInfo
		{
			set { _additionalInfo = value; }
			get { return _additionalInfo; }
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when filling a new Weapon from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public Weapon(Hashtable newValues)
		{
			this.TypeID = (int)newValues["FirearmTypeID"];
			this.MakeTypeID = (int)newValues["FireArmMakeTypeID"];
			this.Model = (string)newValues["Model"];
			this.CaliberTypeID = (int)newValues["FirearmCaliberTypeID"];
			this.SerialNumber = (string)newValues["SerialNumber"];
			this.NcicIsStolen = (bool)newValues["NcicIsStolen"];
			this.NcicContactName = (string)newValues["NcicContactName"];
			this.HasPawnshopRecord = (bool)newValues["HasPawnshopRecord"];
			this.PawnshopContactName = (string)newValues["PawnshopContactName"];
			this.AdditionalInfo = (string)newValues["AdditionalInfo"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new Weapon from datarow. (ex: record from db)
		/// </summary>
		public Weapon(DataRow row)
		{
			FillObject(this, row);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo Add(string createdBy)
		{
			return WeaponDA.InsertWeapon(this.ReportID,
										 this.TypeID,
										 this.MakeTypeID,
										 this.Model,
										 this.CaliberTypeID,
										 this.SerialNumber,
										 this.NcicIsStolen,
										 this.NcicContactName,
										 this.HasPawnshopRecord,
										 this.PawnshopContactName,
										 this.AdditionalInfo,
										 createdBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo Update(string modifiedBy)
		{
			return WeaponDA.UpdateWeapon(this.ID,
										 this.TypeID,
										 this.MakeTypeID,
										 this.Model,
										 this.CaliberTypeID,
										 this.SerialNumber,
										 this.NcicIsStolen,
										 this.NcicContactName,
										 this.HasPawnshopRecord,
										 this.PawnshopContactName,
										 this.AdditionalInfo,
										 modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static StatusInfo DeleteWeapon(int weaponID)
		{
			return WeaponDA.DeleteWeapon(weaponID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetWeaponsByReport(int reportID)
		{
			return WeaponDA.GetWeaponsByReport(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmTypes()
		{
			const string KEYNAME = "dtFirearmTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = WeaponDA.GetFirearmTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmMakeTypes()
		{
			return WeaponDA.GetFirearmMakeTypes();

			//NOTE: not caching this due to its huge size (2000 rows). instead: load-on-demand from page.

			//const string KEYNAME = "dtFirearmMakeTypes";

			//Cache cache = HttpRuntime.Cache;

			//if (cache[KEYNAME] == null) //not in session, create
			//{
			//    DataTable results = WeaponDA.GetFirearmMakeTypes();

			//    cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			//}

			////return table (which is already in cache or brand new)
			//return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmModelTypes()
		{
			const string KEYNAME = "dtFirearmModelTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = WeaponDA.GetFirearmModelTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetFirearmCaliberTypes()
		{
			const string KEYNAME = "dtFirearmCaliberTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = WeaponDA.GetFirearmCaliberTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific VictimPerson, via constructor.
		/// </summary>
		private void FillObject(Weapon weapon, DataRow row)
		{
			//required
			weapon.ID = (int)row["WeaponID"];
			weapon.ReportID = (int)row["ReportID"];
			weapon.TypeID = (int)row["FirearmTypeID"];
			weapon.Type = (string)row["Type"];

			if (row["FirearmMakeTypeID"] != DBNull.Value)
				weapon.MakeTypeID = (int)row["FirearmMakeTypeID"];

			if (row["Make"] != DBNull.Value)
				weapon.Make = (string)row["Make"];

			if (row["Model"] != DBNull.Value)
				weapon.Model = (string)row["Model"];

			if (row["FirearmCaliberTypeID"] != DBNull.Value)
				weapon.CaliberTypeID = (int)row["FirearmCaliberTypeID"];

			if (row["Caliber"] != DBNull.Value)
				weapon.Caliber = (string)row["Caliber"];

			if (row["SerialNumber"] != DBNull.Value)
				weapon.SerialNumber = (string)row["SerialNumber"];

			if (row["NcicIsStolen"] != DBNull.Value)
				weapon.NcicIsStolen = (bool)row["NcicIsStolen"];

			if (row["NcicContactName"] != DBNull.Value)
				weapon.NcicContactName = (string)row["NcicContactName"];

			if (row["HasPawnshopRecord"] != DBNull.Value)
				weapon.HasPawnshopRecord = (bool)row["HasPawnshopRecord"];

			if (row["PawnshopContactName"] != DBNull.Value)
				weapon.PawnshopContactName = (string)row["PawnshopContactName"];

			if (row["AdditionalInfo"] != DBNull.Value)
				weapon.AdditionalInfo = (string)row["AdditionalInfo"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
