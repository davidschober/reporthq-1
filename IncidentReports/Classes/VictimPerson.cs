using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using ReportHq.Common;
using ReportHq.IncidentReports.DataAccess;

namespace ReportHq.IncidentReports
{
	public class VictimPerson
	{
		#region Properties

		private int _ID;
		private int _reportID;
		private int _victimNumber;
		private int _victimPersonTypeID;
		private string _victimPersonType;
		private int _victimTypeID;
		private string _victimType;
		private string _lastName;
		private string _firstName;
		private int _raceTypeID;
		private string _race;
		private int _genderTypeID;
		private string _gender;
		private DateTime _dateOfBirth;
		private string _streetAddress;
		private string _city;
		private string _state;
		private int _zipCode;
		private string _phoneNumber;
		private string _socialSecurityNumber;
		private string _driversLicenseNumber;
		private int _sobrietyTypeID;
		private string _sobriety;
		private int _injuryTypeID;
		private string _injury;
		private int _treatedTypeID;
		private string _treated;
		private string _occupation;
		private string _workStreetAddress;
		private string _workCity;
		private string _workState;
		private int _workZipCode;
		private string _workPhoneNumber;
		private string _emailAddress;

		public int ID
		{
			set { _ID = value; }
			get { return _ID; }
		}

		public int ReportID
		{
			set { _reportID = value; }
			get { return _reportID; }
		}

		public int VictimNumber
		{
			set { _victimNumber = value; }
			get { return _victimNumber; }
		}

		public int VictimPersonTypeID
		{
			set { _victimPersonTypeID = value; }
			get { return _victimPersonTypeID; }
		}

		public string VictimPersonType
		{
			set { _victimPersonType = value; }
			get { return _victimPersonType; }
		}

		public int VictimTypeID
		{
			set { _victimTypeID = value; }
			get { return _victimTypeID; }
		}

		public string VictimType
		{
			set { _victimType = value; }
			get { return _victimType; }
		}

		public string LastName
		{
			set { _lastName = value; }
			get { return _lastName; }
		}

		public string FirstName
		{
			set { _firstName = value; }
			get { return _firstName; }
		}

		public int RaceTypeID
		{
			set { _raceTypeID = value; }
			get { return _raceTypeID; }
		}

		public string Race
		{
			set { _race = value; }
			get { return _race; }
		}

		public int GenderTypeID
		{
			set { _genderTypeID = value; }
			get { return _genderTypeID; }
		}

		public string Gender
		{
			set { _gender = value; }
			get { return _gender; }
		}

		public DateTime DateOfBirth
		{
			set { _dateOfBirth = value; }
			get { return _dateOfBirth; }
		}

		public string StreetAddress
		{
			set { _streetAddress = value; }
			get { return _streetAddress; }
		}

		public string City
		{
			set { _city = value; }
			get { return _city; }
		}

		public string State
		{
			set { _state = value; }
			get { return _state; }
		}

		public int ZipCode
		{
			set { _zipCode = value; }
			get { return _zipCode; }
		}

		public string PhoneNumber
		{
			set { _phoneNumber = value; }
			get { return _phoneNumber; }
		}

		public string SocialSecurityNumber
		{
			set { _socialSecurityNumber = value; }
			get { return _socialSecurityNumber; }
		}

		public string DriversLicenseNumber
		{
			set { _driversLicenseNumber = value; }
			get { return _driversLicenseNumber; }
		}

		public int SobrietyTypeID
		{
			set { _sobrietyTypeID = value; }
			get { return _sobrietyTypeID; }
		}

		public string Sobriety
		{
			set { _sobriety = value; }
			get { return _sobriety; }
		}

		public int InjuryTypeID
		{
			set { _injuryTypeID = value; }
			get { return _injuryTypeID; }
		}

		public string Injury
		{
			set { _injury = value; }
			get { return _injury; }
		}

		public int TreatedTypeID
		{
			set { _treatedTypeID = value; }
			get { return _treatedTypeID; }
		}

		public string Treated
		{
			set { _treated = value; }
			get { return _treated; }
		}

		public string Occupation
		{
			set { _occupation = value; }
			get { return _occupation; }
		}

		public string WorkStreetAddress
		{
			set { _workStreetAddress = value; }
			get { return _workStreetAddress; }
		}

		public string WorkCity
		{
			set { _workCity = value; }
			get { return _workCity; }
		}

		public string WorkState
		{
			set { _workState = value; }
			get { return _workState; }
		}

		public int WorkZipCode
		{
			set { _workZipCode = value; }
			get { return _workZipCode; }
		}

		public string WorkPhoneNumber
		{
			set { _workPhoneNumber = value; }
			get { return _workPhoneNumber; }
		}

		public string EmailAddress
		{
			set { _emailAddress = value; }
			get { return _emailAddress; }
		}
		

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Constructors

		/// <summary>
		/// Use this when requesting a specific VictimPerson.
		/// </summary>
		public VictimPerson(int victimPersonID)
		{
			DataSet ds = VictimPersonDA.GetVictimPersonByID(victimPersonID);

			if (ds.Tables[0].Rows.Count > 0)
				FillObject(this, ds.Tables[0].Rows[0]);
			//else
			//throw new Exception("Could not locate a Victim Person for VictimPersonID " + victimPersonID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new VictimPerson from values collected from a Grid. (ex: an insert operation)
		/// </summary>
		public VictimPerson(Hashtable newValues)
		{
			this.VictimNumber = (int)newValues["VictimNumber"];
			this.VictimPersonTypeID = (int)newValues["VictimPersonTypeID"];
			this.VictimTypeID = (int)newValues["VictimTypeID"];
			this.LastName = (string)newValues["LastName"];
			this.FirstName = (string)newValues["FirstName"];
			this.RaceTypeID = (int)newValues["RaceTypeID"];
			this.GenderTypeID = (int)newValues["GenderTypeID"];

			if (newValues["DateOfBirth"] != null)
				this.DateOfBirth = (DateTime)newValues["DateOfBirth"];

			this.StreetAddress = (string)newValues["StreetAddress"];
			this.City = (string)newValues["City"];
			this.State = (string)newValues["State"];
			this.ZipCode = (int)newValues["ZipCode"];
			this.PhoneNumber = (string)newValues["PhoneNumber"];
			this.SocialSecurityNumber = (string)newValues["SocialSecurityNumber"];
			this.DriversLicenseNumber = (string)newValues["DriversLicenseNumber"];
			this.SobrietyTypeID = (int)newValues["SobrietyTypeID"];
			this.InjuryTypeID = (int)newValues["InjuryTypeID"];
			this.TreatedTypeID = (int)newValues["TreatedTypeID"];
			this.Occupation = (string)newValues["Occupation"];
			this.WorkStreetAddress = (string)newValues["WorkStreetAddress"];
			this.WorkCity = (string)newValues["WorkCity"];
			this.WorkState = (string)newValues["WorkState"];
			this.WorkZipCode = (int)newValues["WorkZipCode"];
			this.WorkPhoneNumber = (string)newValues["WorkPhoneNumber"];
			this.EmailAddress = (string)newValues["EmailAddress"];
		}
		
		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Use this when filling a new VictimPerson from datarow. (ex: record from db)
		/// </summary>
		public VictimPerson(DataRow row)
		{
			FillObject(this, row);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Methods

		public StatusInfo Add(string createdBy)
		{
			return VictimPersonDA.InsertVictimPerson(this.ReportID,
													 this.VictimNumber,
													 this.VictimPersonTypeID,
													 this.VictimTypeID,
													 this.LastName,
													 this.FirstName,
													 this.RaceTypeID,
													 this.GenderTypeID,
													 this.DateOfBirth,
													 this.StreetAddress,
													 this.City,
													 this.State,
													 this.ZipCode,
													 this.PhoneNumber,
													 this.SocialSecurityNumber,
													 this.DriversLicenseNumber,
													 this.SobrietyTypeID,
													 this.InjuryTypeID,
													 this.TreatedTypeID,
													 this.Occupation,
													 this.WorkStreetAddress,
													 this.WorkCity,
													 this.WorkState,
													 this.WorkZipCode,
													 this.WorkPhoneNumber,
													 this.EmailAddress,
													 createdBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public StatusInfo Update(string modifiedBy)
		{
			return VictimPersonDA.UpdateVictimPerson(this.ID,
													 this.VictimNumber,
													 this.VictimPersonTypeID,
													 this.VictimTypeID,
													 this.LastName,
													 this.FirstName,
													 this.RaceTypeID,
													 this.GenderTypeID,
													 this.DateOfBirth,
													 this.StreetAddress,
													 this.City,
													 this.State,
													 this.ZipCode,
													 this.PhoneNumber,
													 this.SocialSecurityNumber,
													 this.DriversLicenseNumber,
													 this.SobrietyTypeID,
													 this.InjuryTypeID,
													 this.TreatedTypeID, 
													 this.Occupation,
													 this.WorkStreetAddress,
													 this.WorkCity,
													 this.WorkState,
													 this.WorkZipCode,
													 this.WorkPhoneNumber,
													 this.EmailAddress,
													 modifiedBy);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static StatusInfo DeleteVictimPerson(int victimPersonID)
		{
			return VictimPersonDA.DeleteVictimPerson(victimPersonID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Public Static Methods

		public static DataTable GetVictimPersonTypes()
		{
			const string KEYNAME = "dtVictimPersonTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VictimPersonDA.GetVictimPersonTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimTypes()
		{
			const string KEYNAME = "dtVictimTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VictimPersonDA.GetVictimTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		public static DataTable GetVictimsByReport(int reportID)
		{
			return VictimPersonDA.GetVictimsByReport(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Victim-to-Offender relation types. ie, "Victim was..."
		/// </summary>
		/// <returns></returns>
		public static DataTable GetVictimOffenderRelationshipTypes()
		{
			const string KEYNAME = "dtVictimOffenderRelationshipTypes";

			Cache cache = HttpRuntime.Cache;

			if (cache[KEYNAME] == null) //not in session, create
			{
				DataTable results = VictimPersonDA.GetVictimOffenderRelationshipTypes();

				cache.Insert(KEYNAME, results, null, DateTime.Now.AddDays(3), TimeSpan.Zero);
			}

			//return table (which is already in cache or brand new)
			return (DataTable)cache[KEYNAME];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		/// <summary>
		/// Gets a DataTable of a Report's VictimPersons.
		/// </summary>
		public static DataTable GetVictimPersonsByReport(int reportID)
		{
			return VictimPersonDA.GetVictimPersonsByReport(reportID);
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Private Methods

		/// <summary>
		/// Fills this object for a specific VictimPerson, via constructor.
		/// </summary>
		private void FillObject(VictimPerson victimPerson, DataRow row)
		{
			//required
			victimPerson.ID = (int)row["VictimPersonID"];
			victimPerson.ReportID = (int)row["ReportID"];
			victimPerson.VictimNumber = (int)row["VictimNumber"];
			victimPerson.VictimPersonTypeID = (int)row["VictimPersonTypeID"];
			victimPerson.VictimPersonType = (string)row["VictimPersonType"];

			if (row["VictimTypeID"] != DBNull.Value)
				victimPerson.VictimTypeID = (int)row["VictimTypeID"];

			if (row["VictimType"] != DBNull.Value)
				victimPerson.VictimType = (string)row["VictimType"];
			
			if (row["LastName"] != DBNull.Value)
				victimPerson.LastName = (string)row["LastName"];

			if (row["FirstName"] != DBNull.Value)
				victimPerson.FirstName = (string)row["FirstName"];

			if (row["RaceTypeID"] != DBNull.Value)
				victimPerson.RaceTypeID = (int)row["RaceTypeID"];

			if (row["Race"] != DBNull.Value)
				victimPerson.Race = (string)row["Race"];

			if (row["GenderTypeID"] != DBNull.Value)
				victimPerson.GenderTypeID = (int)row["GenderTypeID"];

			if (row["Gender"] != DBNull.Value)
				victimPerson.Gender = (string)row["Gender"];

			if (row["DateOfBirth"] != DBNull.Value)
				victimPerson.DateOfBirth = (DateTime)row["DateOfBirth"];

			if (row["StreetAddress"] != DBNull.Value)
				victimPerson.StreetAddress = (string)row["StreetAddress"];

			if (row["City"] != DBNull.Value)
				victimPerson.City = (string)row["City"];

			if (row["State"] != DBNull.Value)
				victimPerson.State = (string)row["State"];

			if (row["ZipCode"] != DBNull.Value)
				victimPerson.ZipCode = (int)row["ZipCode"];

			if (row["PhoneNumber"] != DBNull.Value)
				victimPerson.PhoneNumber = (string)row["PhoneNumber"];

			if (row["SocialSecurityNumber"] != DBNull.Value)
				victimPerson.SocialSecurityNumber = (string)row["SocialSecurityNumber"];

			if (row["DriversLicenseNumber"] != DBNull.Value)
				victimPerson.DriversLicenseNumber = (string)row["DriversLicenseNumber"];

			if (row["SobrietyTypeID"] != DBNull.Value)
				victimPerson.SobrietyTypeID = (int)row["SobrietyTypeID"];

			if (row["Sobriety"] != DBNull.Value)
				victimPerson.Sobriety = (string)row["Sobriety"];

			if (row["InjuryTypeID"] != DBNull.Value)
				victimPerson.InjuryTypeID = (int)row["InjuryTypeID"];

			if (row["Injury"] != DBNull.Value)
				victimPerson.Injury = (string)row["Injury"];

			if (row["TreatedTypeID"] != DBNull.Value)
				victimPerson.TreatedTypeID = (int)row["TreatedTypeID"];

			if (row["Treated"] != DBNull.Value)
				victimPerson.Treated = (string)row["Treated"];

			if (row["Occupation"] != DBNull.Value)
				victimPerson.Occupation = (string)row["Occupation"];

			if (row["WorkStreetAddress"] != DBNull.Value)
				victimPerson.WorkStreetAddress = (string)row["WorkStreetAddress"];

			if (row["WorkCity"] != DBNull.Value)
				victimPerson.WorkCity = (string)row["WorkCity"];

			if (row["WorkState"] != DBNull.Value)
				victimPerson.WorkState = (string)row["WorkState"];

			if (row["WorkZipCode"] != DBNull.Value)
				victimPerson.WorkZipCode = (int)row["WorkZipCode"];

			if (row["WorkPhoneNumber"] != DBNull.Value)
				victimPerson.WorkPhoneNumber = (string)row["WorkPhoneNumber"];

			if (row["EmailAddress"] != DBNull.Value)
				victimPerson.EmailAddress = (string)row["EmailAddress"];
		}

		//'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}
