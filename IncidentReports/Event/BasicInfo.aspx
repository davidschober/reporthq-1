﻿<%@ Page Title="Report HQ - Basic Info" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="BasicInfo.aspx.cs" Inherits="ReportHq.IncidentReports.Event.BasicInfo" %>
<%@ Register TagPrefix="ReportHq" TagName="BasicInfo" src="controls/BasicInfo.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
	<script type="text/javascript" src="../js/radComboBoxHelpers.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="Event" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />

	<ReportHq:BasicInfo ID="ucEvent" OnNavItemClicked="ucBasicInfo_NavItemClicked" runat="server" />

</asp:Content>
