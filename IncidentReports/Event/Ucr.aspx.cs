﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using ReportHq.Common;
using ReportHq.IncidentReports;
using ReportHq.IncidentReports.Controls;
using ReportHq.IncidentReports.Event.Controls;

namespace ReportHq.IncidentReports.Event
{
	public partial class Ucr : Bases.ReportPage
	{
		#region Variables

		private Report _report;

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion

		#region Event Handlers

		protected void Page_Init(object sender, EventArgs e)
		{
			_report = base.Report; //set from base's OnInit
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void Page_Load(object sender, EventArgs e)
		{
			this.HelpUrl = "~/Event/Help/Ucr.html"; 

			//if (!base.UserHasWriteAccess)
			//	ucEvent.DisableValidators = true;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucMajorSections_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportMainNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		protected void ucUcr_NavItemClicked(object sender, EventArgs e)
		{
			base.ProcessNavClick((sender as ReportSubNav).TargetRedirectUrl, this.UserHasWriteAccess);
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		
		#endregion

		#region Public Methods

		/// <summary>
		/// Saves the report. On success, updates Session copy. This overrides the method stub in the base ReportPage, and is called by the base's ProcessNavClick() public method.
		/// </summary>
		public override StatusInfo SaveReport()
		{
			_report = base.Report; //the page base ensures the report is sync'd up w/ the ViewState prior to it calling this SaveReport(). now ensure our local copy is the latest instance.

			//set new form values
			//_report.SignalTypeID = ucEvent.SignalTypeID;
			//_report.IncidentDescription = ucEvent.IncidentDescription;
			
			//update
			//StatusInfo status = _report.UpdateEvent(this.Username);
			StatusInfo status = new StatusInfo(true); //TEMP

			if (status.Success == true)
				base.Report = _report; //update Session copy w/ new values

			return status;
		}

		//''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

		#endregion
	}
}