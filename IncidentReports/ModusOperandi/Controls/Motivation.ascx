﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Motivation.ascx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.Controls.Motivation" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="Motivation" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<div class="formBox" style="width:640px;">

	<table width="100%" border="0" class="bordered">
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Motive</div>
				<asp:CheckBoxList ID="cblMotives" DataValueField="MotivationTypeID" DataTextField="Description" EnableViewState="False" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Targets</div>
				<asp:CheckBoxList ID="cblTargets" DataValueField="MotivationTypeID" DataTextField="Description" EnableViewState="False" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</td>
		</tr>
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Criminal Activity</div>
				<asp:CheckBoxList ID="cblCriminalActivity" DataValueField="MotivationTypeID" DataTextField="Description" EnableViewState="False" RepeatLayout="Flow" runat="server" />
			</td>
		</tr>
	</table><br />
	
	<div class="label">"Other" Info:</div>
	<asp:TextBox ID="tbMotivationDesc" TextMode="MultiLine" Rows="5" Columns="50" EnableViewState="False" MaxLength="500" onkeypress="return isMaxLength(this, 500);" onblur="trimSize(this, 500);" runat="server"/><br />
	
</div>