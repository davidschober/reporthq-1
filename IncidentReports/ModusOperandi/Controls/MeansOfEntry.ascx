﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MeansOfEntry.ascx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.Controls.MeansOfEntry" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="MeansOfEntry" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />
	
<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<div class="formBox" style="width:640px;">

	<table width="100%" border="0" class="bordered">
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Method of Entry</div>			
				<asp:CheckBoxList ID="cblMethodsOfEntry" DataValueField="MeansOfEntryTypeID" DataTextField="Description" EnableViewState="false" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</td>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel">Point of Entry/Exit</div>			
				<asp:CheckBoxList ID="cblPointsOfEntry" DataValueField="MeansOfEntryTypeID" DataTextField="Description" EnableViewState="false" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
			</td>
		</tr>	
		<tr>
			<td valign="top" class="bordered">
				<div class="checkBoxListLabel" style="text-align:center;">Security Used/Defeated</div>
			
				<style type="text/css">
					.line {
						border-bottom:solid;
						border-width:1px;
						border-color:#999999;
					}

					.alt td {
						background-color:#C5D4E2;
					}

				</style>
			
				<asp:Repeater ID="rptSecurityTypesUsed" EnableViewState="false" runat="server">
					<HeaderTemplate>
						<table width="100%" cellpadding="0" cellspacing="0" class="center">
					</HeaderTemplate>				
					<ItemTemplate>
						<tr align="left">
							<td>
								<asp:HiddenField ID="hidMeansOfEntryTypeID" Value='<%# Eval("MeansOfEntryTypeID") %>' runat="server" />
								<asp:CheckBox ID="cbWasUsed" Text='<%# Eval("Description") %>' runat="server" />
							</td>
							<td><img src="/1.gif" width="10" height="1" /></td>
							<td><asp:CheckBox ID="cbWasDefeated" runat="server" Text="Defeated" /></td>
						</tr>
					</ItemTemplate>
					<AlternatingItemTemplate>
						<tr align="left" class="alt">
							<td>
								<asp:HiddenField ID="hidMeansOfEntryTypeID" Value='<%# Eval("MeansOfEntryTypeID") %>' runat="server" />
								<asp:CheckBox ID="cbWasUsed" Text='<%# Eval("Description") %>' runat="server" />
							</td>
							<td><img src="/1.gif" width="10" height="1" /></td>
							<td><asp:CheckBox ID="cbWasDefeated" runat="server" Text="Defeated" /></td>
						</tr>
					</AlternatingItemTemplate>
					<FooterTemplate>
						</table>
					</FooterTemplate>
				</asp:Repeater>
			
			</td>
		</tr>
	</table><br />
	
	<div class="label">"Other" Info:</div>
	<asp:TextBox ID="tbMeansOfEntryDesc" TextMode="MultiLine" Rows="5" Columns="50" EnableViewState="False" MaxLength="500" onkeypress="return isMaxLength(this, 500);" onblur="trimSize(this, 500);" runat="server"/><br />
	
</div>