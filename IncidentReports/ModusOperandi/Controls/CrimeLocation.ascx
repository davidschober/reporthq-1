﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CrimeLocation.ascx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.Controls.CrimeLocation" %>
<%@ Register TagPrefix="ReportHq" TagName="ReportSubNav" src="ReportSubNav.ascx" %>

<ReportHq:ReportSubNav ID="ucReportSubNav" CurrentSection="CrimeLocation" OnNavItemClicked="ucReportSubNav_NavItemClicked" runat="server" />

<Telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="CheckBoxes" Skin="Metro" runat="server" />

<div class="formBox" style="width:640px;">

	<table width="100%" border="0" class="bordered">
	<tr>
		<td valign="top" class="bordered">
			<div class="checkBoxListLabel">Residential</div>			
			<asp:CheckBoxList ID="cblResidential" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
		</td>
		<td colspan="2" valign="top" class="bordered">
			<div class="checkBoxListLabel">Commerical Establishment</div>			
			<asp:CheckBoxList ID="cblCommerical" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatColumns="3" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
		</td>
	</tr>
	<tr>
		<td valign="top" class="bordered">
			<div class="checkBoxListLabel">Outdoor Area</div>			
			<asp:CheckBoxList ID="cblOutdoorAreas" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
		</td>
		<td colspan="2" valign="top" class="bordered">
			<div class="checkBoxListLabel">Public Access Area</div>			
			<asp:CheckBoxList ID="cblPublicAccessAreas" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatColumns="2" CellPadding="0" CellSpacing="0" Width="100%" runat="server" />
		</td>
	</tr>
	<tr>
		<td valign="top" class="bordered">
			<div class="checkBoxListLabel">Movable</div>			
			<asp:CheckBoxList ID="cblMoveables" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
		</td>
		<td valign="top" class="bordered">
			<div class="checkBoxListLabel">Structure Type</div>			
			<asp:CheckBoxList ID="cblStructureTypes" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
		</td>
		<td valign="top" class="bordered">
			<div class="checkBoxListLabel">Structure Status</div>			
			<asp:CheckBoxList ID="cblStructureStatuses" DataValueField="CrimeLocationTypeID" DataTextField="Description" EnableViewState="false" RepeatLayout="Flow" runat="server" />
		</td>
	</tr>
</table><br />
	
	<div class="label">"Other" Info:</div>
	<asp:TextBox ID="tbCrimeLocationDesc" TextMode="MultiLine" Rows="5" Columns="50" EnableViewState="False" MaxLength="500" onkeypress="return isMaxLength(this, 500);" onblur="trimSize(this, 500);" runat="server"/><br />
	
</div>