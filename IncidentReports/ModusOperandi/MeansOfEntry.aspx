﻿<%@ Page Title="Report HQ - Means of Entry" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="MeansOfEntry.aspx.cs" Inherits="ReportHq.IncidentReports.ModusOperandi.MeansOfEntry" %>
<%@ Register TagPrefix="ReportHq" TagName="MeansOfEntry" src="controls/MeansOfEntry.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="../css/reportForm.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">

	<ReportHq:ReportMainNav id="ucReportMainNav" CurrentSection="ModusOperandi" OnNavItemClicked="ucMajorSections_NavItemClicked" runat="server" />
	
	<ReportHq:MeansOfEntry ID="ucMeansOfEntry" OnNavItemClicked="ucMeansOfEntry_NavItemClicked" runat="server" />

</asp:Content>
