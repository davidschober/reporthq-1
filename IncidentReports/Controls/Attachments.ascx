﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attachments.ascx.cs" Inherits="ReportHq.IncidentReports.Controls.Attachments" %>

<telerik:RadFormDecorator id="radFormDecorator" DecoratedControls="Buttons" Skin="Silk" runat="server" />

<telerik:RadAjaxPanel ID="radAjaxPanel" LoadingPanelID="ajaxLoadingPanel" runat="server">

	<!-- pictures -->
	<dl class="box reportForm" style="margin-top:-1px; margin-bottom:30px;">
		<dt>
			<h3>Pictures</h3>
		</dt>
		<dd>
			
			<asp:Repeater ID="rptPictures" runat="server">
				<HeaderTemplate>
					<table width="100%" cellspacing="0" border="0" class="lightList">
						<thead>
							<tr>
								<th width="40"></th>
								<th>FILE NAME</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<!-- href='print/Attachment.aspx?fileType=jpg&file=<%# Eval("SavedPath") %>' -->
						<td><a title="Open JPG" href="#" onclick="openLightbox(<%# Container.ItemIndex + 1 %>);return false;"><img src="images/img.gif" width="16" height="16"/></a></td>
						<td><a title="Open JPG" href="#" onclick="openLightbox(<%# Container.ItemIndex + 1 %>);return false;"><%# System.IO.Path.GetFileName((string)Eval("SavedPath"))%></a></td>
						<td><asp:LinkButton ID="lbDelete" Text="Delete" OnClientClick="return DeleteConfirmation();" OnCommand="lbDelete_Command" CommandArgument='<%#Eval("ID")%>' runat="server" /></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
						</tbody>
					</table>
				</FooterTemplate>
			</asp:Repeater>
			<asp:Literal ID="litNonePictures" Text="The report has no pictures." Visible="false" runat="server" />

		</dd>
	</dl>


	<!-- pdfs -->
	<dl class="box reportForm" style="margin-top:-1px; margin-bottom:30px; height:initial; min-height:320px;">
		<dt>
			<h3>Documents</h3>
		</dt>
		<dd>				
			
			<asp:Repeater ID="rptPdfs" runat="server">
				<HeaderTemplate>
					<table width="100%" cellspacing="0" border="0" class="lightList">
						<thead>
							<tr>
								<th width="40"></th>
								<th>FILE NAME</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><a href='print/Attachment.aspx?fileType=pdf&file=<%# Eval("SavedPath") %>' title="Open PDF"><img src="images/pdf.gif" width="16" height="16"/></a></td>
						<td><a href='print/Attachment.aspx?fileType=pdf&file=<%# Eval("SavedPath") %>' title="Open PDF"><%# System.IO.Path.GetFileName((string)Eval("SavedPath"))%></a></td>
						<td><asp:LinkButton ID="lbDelete" Text="Delete" OnClientClick="return DeleteConfirmation();" OnCommand="lbDelete_Command" CommandArgument='<%#Eval("ID")%>' runat="server" /></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
						</tbody>
					</table>
				</FooterTemplate>
			</asp:Repeater>
			<asp:Literal ID="litNonePdfs" Text="The report has no PDFs." Visible="false" runat="server" />

		</dd>
	</dl>

	<div style="clear:both;"></div>

	<div id="divUploader" runat="server">
		<p><b>Allowed file types:</b> .jpg, .jpeg, .pdf</p>

		<telerik:RadAsyncUpload ID="radUpload" 
			MultipleFileSelection="Automatic" 
			AllowedFileExtensions="jpg,jpeg,pdf" 
			MaxFileSize="1048576" 					 
			OnFileUploaded="radUpload_FileUploaded"
			runat="server" /><br />

		<asp:LinkButton ID="lbSave" CssClass="flatButton" runat="server"><i class="fa fa-check fa-fw"></i> Save Files</asp:LinkButton>
	</div>

</telerik:RadAjaxPanel>

<telerik:RadLightBox ID="lightBox" LoopItems="true" ClientIDMode="Static" runat="server">
	<ClientSettings AllowKeyboardNavigation="true" ContentResizeMode="Fit" NavigationMode="Zone" />
</telerik:RadLightBox>

<script type="text/javascript">

	function openLightbox(i) {
		var lightBox = $find('lightBox');
		//alert(i);

		lightBox.navigateTo(i-1);
		lightBox.show();
	}

	function DeleteConfirmation() {
		return confirm("Are you *sure* you want to delete it? This cannot be undone.");
	}

</script>