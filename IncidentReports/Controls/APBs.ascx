﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="APBs.ascx.cs" Inherits="ReportHq.IncidentReports.Controls.APBs" %>

<dl class="box subtext fixed">
	<dt>
		<i class="iconExclaim"></i>
		<div class="description">
			<h3>APBs</h3>
			All-Point Bulletins
		</div>
	</dt>
	<dd>
		<asp:Repeater ID="rptApb" runat="server">
			<HeaderTemplate>
				<table width="100%" cellspacing="0" border="0" class="lightList">
					<thead>
						<tr>
							<th>DATE</th>
							<th>TITLE</th>
						</tr>
					</thead>
					<tbody>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%# string.Format("{0:MM/dd/yyyy}", Eval("CreatedDate"))%></a></td>
					<td><a href="javascript:openBulletin(<%#Eval("ID")%>);" title="Open the item"><%#Eval("Title")%></a></td>
				</tr>
			</ItemTemplate>
			<FooterTemplate>
					</tbody>
				</table>
			</FooterTemplate>
		</asp:Repeater>
		<asp:Literal ID="litNoneApb" Text="None at this time." Visible="false" runat="server" />
	</dd>
</dl>