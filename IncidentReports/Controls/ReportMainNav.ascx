﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportMainNav.ascx.cs" Inherits="ReportHq.IncidentReports.Controls.ReportMainNav" %>

<!--
<ul class="reportNav">
	<li class="selected">Event</li>
	<li><a href="#">People</a></li>
	<li><a href="#">Property</a></li>
	<li><a href="#">M.O.</a></li>
	<li><a href="#">Narrative</a></li>
	<li><a href="#">Attachments</a></li>
</ul>

	<p>&nbsp;</p>
-->

<ul class="reportNav">
	<li id="liEvent" runat="server"><asp:Literal ID="litEvent" Text="Event" Visible="false" runat="server" /><asp:LinkButton ID="lbEvent" Text="Event" ToolTip="Save your work and move here" OnClick="lbEvent_Click" runat="server" /></li>
	<li id="liPeople" runat="server"><asp:Literal ID="litPeople" Text="People" Visible="false" runat="server" /><asp:LinkButton ID="lbPeople" Text="People" ToolTip="Save your work and move here" OnClick="lbPeople_Click" runat="server" /></li>
	<li id="liProperty" runat="server"><asp:Literal ID="litProperty" Text="Property" Visible="false" runat="server" /><asp:LinkButton ID="lbProperty" Text="Property" ToolTip="Save your work and move here" OnClick="lbProperty_Click" runat="server" /></li>
	<li id="liMo" runat="server"><asp:Literal ID="litMo" Text="M.O." Visible="false" runat="server" /><asp:LinkButton ID="lbMo" Text="M.O." ToolTip="Save your work and move here" OnClick="lbMo_Click" runat="server" /></li>
	<li id="liNarrative" runat="server"><asp:Literal ID="litNarrative" Text="Narrative" Visible="false" runat="server" /><asp:LinkButton ID="lbNarrative" Text="Narrative" ToolTip="Save your work and move here" OnClick="lbNarrative_Click" runat="server" /></li>
	<li id="liAttachments" runat="server"><asp:Literal ID="litAttachments" Text="Attachments" Visible="false" runat="server" /><asp:LinkButton ID="lbAttachments" Text="Attachments" ToolTip="Save your work and move here" OnClick="lbAttachments_Click" runat="server" /></li>
</ul>
