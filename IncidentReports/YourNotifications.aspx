﻿<%@ Page Title="Your Notifications" Language="C#" MasterPageFile="~/IncidentReports.Master" AutoEventWireup="true" CodeBehind="YourNotifications.aspx.cs" Inherits="ReportHq.IncidentReports.YourNotifications" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link rel="stylesheet" href="css/boxes.css" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="phContent" runat="server">
		
	<!-- reports -->
	<dl class="box subtext" style="height:initial;">
		<dt>
			<i class="iconFont fa fa-file-text-o"></i>
			<%--<i class="iconExclaim"></i>--%>
			<div class="description">
				<h3><a href="YourReports.aspx">Report Notifications</a></h3>
				These notifications are for your reports
			</div>
		</dt>
		<dd>
			<asp:Repeater ID="rptReports" runat="server">
				<HeaderTemplate>
					<table width="100%" cellspacing="0" border="0" class="lightList">
						<thead>
							<tr>
								<th></th>
								<th>DATE</th>
								<th>MESSAGE</th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><a href="javascript:alert('open PDF');" title="Open PDF"><img src="images/pdf.gif" width="16" height="16"/></a></td>
						<td><%# DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:MM/dd/yyyy}") %></td>
						<td><a href="event/BasicInfo.aspx?rid=<%#Eval("RelatedObjectID")%>" title="Go to Report"><%#Eval("Message")%></a></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
						</tbody>
					</table>
				</FooterTemplate>
			</asp:Repeater>
			<asp:Literal ID="litNoneReports" Text="You have no notifications." Visible="false" runat="server" />
		</dd>
	</dl>
			
		
	<!-- global -->
	<dl class="box subtext" style="height:initial;">
		<dt>				
			<i class="iconFont fa fa-globe"></i>
			<div class="description">
				<h3><a href="RollCall.aspx">Global Notifications</a></h3>
				Other notifications
			</div>
		</dt>
		<dd>
			<asp:Repeater ID="rptGlobal" runat="server">
				<HeaderTemplate>
					<table width="100%" cellspacing="0" border="0" class="lightList">
						<thead>
							<tr>
								<th>DATE</th>
								<th>MESSAGE</th>
							</tr>
						</thead>
						<tbody>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# DataBinder.Eval(Container.DataItem, "CreatedDate", "{0:MM/dd/yyyy}") %></td>
						<td><a href="<%#Eval("Url")%>" title="Go to Item"><%#Eval("Message")%></a></td>
					</tr>
				</ItemTemplate>
				<FooterTemplate>
						</tbody>
					</table>
				</FooterTemplate>
			</asp:Repeater>
			<asp:Literal ID="litNoneGlobal" Text="You have no notifications." Visible="false" runat="server" />
		</dd>
	</dl>

</asp:Content>
